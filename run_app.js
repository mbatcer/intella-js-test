"use strict";

const _ = require("./node_modules/lodash/lodash"),
      fetch = require("node-fetch"),
      express = require("express");

const utils = require("./utils"),
      formatters = require("./formatters");

const sort = require("./sort"),
      aggregate = require("./aggregate");

const PORT_NUMBER = 8000;
const ACTIONS_SORT = "sort";
const ACTIONS_AGGR = "aggregate";
const FORMATS_CSV = "csv";
const FORMATS_SQL = "sql";

const app = express();

app.get("/:action", function(req, res) {
    const action = req.params.action;
    if (!(action === ACTIONS_SORT || action === ACTIONS_AGGR)) {
        throw `Wrong action (must be either ${ACTIONS_SORT} or ${ACTIONS_AGGR})`;
    }
    const { sourceUrl, outputFormat, separator, tableName } = req.query;
    if (!sourceUrl) {
        throw "sourceUrl is not set";
    }
    if (!outputFormat) {
        throw "outputFormat is not set";
    }
    if (!(outputFormat === FORMATS_CSV || outputFormat === FORMATS_SQL)) {
        throw `Wrong outputFormat (must be either ${FORMATS_CSV} or ${FORMATS_SQL})`;
    }
    console.log(`Action: ${action}, URL: ${sourceUrl}, format: ${outputFormat}`);
    fetch(sourceUrl)
        .then(response => response.json())
        .then(sourceJson => {
            const itemsArray = utils.convertToArray(sourceJson);

            let format;
            if (outputFormat.toLowerCase() === FORMATS_CSV) {
                if (!separator) {
                    throw "separator for CSV is not set";
                }
                format = formatters.createCsvFormatter({ separator });
            } else {
                console.log(2);
                if (!tableName) {
                    throw "tableName for SQL is not set";
                }
                format = formatters.createSqlFormatter({ tableName });
            }

            let output;
            if (action === ACTIONS_SORT) {
                output = sort(itemsArray, format);
            } else if (action === ACTIONS_AGGR) {
                output = aggregate(itemsArray, format);
            }
            console.log(output);
            res.send(output);
        })
        .catch(err => {
            console.log(err);
            res.send(err);
        });
});

app.listen(PORT_NUMBER, () => console.log(`App is listening on port ${PORT_NUMBER}`));
