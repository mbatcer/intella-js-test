### Test 1 and 2 ###
Run the web app with `node run_app`. 
For sorting (test 1) and aggregating (test 2), there are two separate endpoints: http://localhost:8000/sort and http://localhost:8000/aggregate

You should pass parameters in URL query:

`sourceURL=http://www.reddit.com/r/javascript/.json`

`outputFormat=sql` or `outputFormat=csv`

and options for the selected format: 

for SQL: `tableName=reddit`

for CSV: `separator=,`

### Test 3 ###
Rut it with `node tree`.