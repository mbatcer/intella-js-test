"use strict";

const _ = require("./node_modules/lodash/lodash");

const SORT_IN_DESC_ORDER = false;

function convertToArray(sourceJson) {
    const contentPropertyName = "data";
    return _.map(sourceJson[contentPropertyName].children, contentPropertyName);
}

function pickPropertiesFromItems(itemsArray, ...properties) {
    return itemsArray.map(item => _.pick(item, _.flatten(properties)));
}

function sortItems(itemsArray, { byProperties, descOrder=SORT_IN_DESC_ORDER }) {
    const sorted = _.sortBy(itemsArray, byProperties);
    return descOrder ? sorted.reverse() : sorted;
}

module.exports = {
    convertToArray,
    pickPropertiesFromItems,
    sortItems
};
