"use strict";

const processors = require("./processors");

function sort(itemsArray, format) {
    const sortByScore = processors.createSortingFunction({ byProperties: "score", descOrder: true });
    const sortedData = sortByScore(itemsArray);
    const result = format(sortedData);
    console.log(result);
    return result;
}

module.exports = sort;
