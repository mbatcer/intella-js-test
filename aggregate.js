"use strict";

const processors = require("./processors");

function aggregate(itemsArray, format) {
    const aggregateByDomain = processors.createAgregationFunction();
    const aggregatedData = aggregateByDomain(itemsArray);
    const result = format(aggregatedData);
    console.log(result);
    return result;
}

module.exports = aggregate;
