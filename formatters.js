"use strict";

const _ = require("./node_modules/lodash/lodash");

function createCsvFormatter(options={ separator: ";" }) {
    const lineEnding = "\n";
    return itemsArray => itemsArray
        .map(item => _.values(item)
            .map(value => typeof(value) === "string" ? `"${value}"` : value)
            .join(`${options.separator} `)
        )
        .join(lineEnding);
}

function createSqlFormatter(options) {
    function columnType(value) {
        if (_.isFinite(value)) {
            return value % 1 === 0 ? "INT" : "FLOAT";
        } else {
            return "VARCHAR(255) CHARACTER SET utf8";
        }
    }
    function rowValues(item) {
        const valuesList = _.values(item)
            .map(value => _.isFinite(value) ? value : `"${value}"`)
            .join(",");
        return `\t(${valuesList})`;
    }
    return itemsArray => {
        const columnNames = _.keys(itemsArray[0]);
        const createTableSql = `CREATE TABLE ${options.tableName} (\n`
            + columnNames.map(name => `\t\`${name}\` ${columnType(itemsArray[0][name])}`).join(",\n")
            + "\n);\n";
        const insertSql = `INSERT INTO ${options.tableName} \n`
            + `\t(${columnNames.join(",")})\n`
            + "VALUES\n"
            + itemsArray.map(item => rowValues(item)).join(",\n")
            + ";";
        return createTableSql + insertSql;
    }
}

module.exports = {
    createCsvFormatter,
    createSqlFormatter
};
