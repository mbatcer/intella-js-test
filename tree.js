"use strict";

const _ = require("./node_modules/lodash/lodash");

const input = [
    { id: 1, parentId: 0 },
    { id: 2, parentId: 0 },
    { id: 3, parentId: 1 },
    { id: 4, parentId: 1 },
    { id: 5, parentId: 2 },
    { id: 6, parentId: 4 },
    { id: 7, parentId: 5 }
];

const tempMap = input.reduce(function(acc, elem) {
    const insertElem = { id: elem.id, parentId: elem.parentId };
    acc.set(insertElem.id, insertElem);
    const parent = acc.get(insertElem.parentId);
    if (parent) {
         if (!parent.children) {
             parent.children = [];
         }
         parent.children.push(insertElem);
    }
    return acc;
}, new Map()); // Maps guarantee order, objects don't

const tree = _.filter(Array.from(tempMap.values()), element => element.parentId === 0);
console.log(JSON.stringify(tree));
