"use strict";

const _ = require("./node_modules/lodash/lodash");
const utils = require("./utils")

function createSortingFunction(options) {
    return function(itemsArray) {
        const sortedItems = utils.sortItems(itemsArray, options);
        const requiredFields = ["id", "title", "created_utc", "score"];
        return utils.pickPropertiesFromItems(sortedItems, requiredFields);
    }
}

function createAgregationFunction() {
    return function(itemsArray) {
        const groupedItems = _.groupBy(itemsArray, "domain");
        const agregated = [];
        _.forOwn(groupedItems, (val, key) => {
            const scoreSum = _.sum(_.map(val, "score"));
            agregated.push({ domain: key, articlesCount: val.length, scoreSum })
        })
        return utils.sortItems(agregated, { byProperties: "articlesCount", descOrder: true });
    }
}

module.exports = {
    createAgregationFunction,
    createSortingFunction
};
